from django.shortcuts import render
from .forms import add_form
from .forms import worker_form
from django.http import HttpResponseRedirect
from .models import ListLocation
from .models import worker
#from .models import ListLocation
response={}
# Create your views here.
def index(request):
    listlocation= ListLocation.objects.all()
    response['listlocation']=listlocation
    html='assign/assign.html'
    response['add_form']=add_form
    return render(request,html,response)
    
def list_location(request):
    form = add_form(request.POST )
    if (request.method == 'POST' ) :
        response['Area'] = request.POST['Area']
        response['Location'] = request.POST['Location']
        listlocation = ListLocation(Area=response['Area'],Location=response['Location'])
        listlocation.save()
        return HttpResponseRedirect('/assign/')
    else:
        return HttpResponseRedirect('/assign/')
        

        
def list_worker(request):
    html='assign/worker.html'
    form = worker_form(request.POST)
    if (request.method == 'POST' ):
        response['name'] = request.POST['name']
        response['email'] = request.POST['email']
        listworker = worker(email=response['email'],name=response['name'])
        listworker.save()
        return render (request, html, response)
    else:
        return render (request, html, response)
        