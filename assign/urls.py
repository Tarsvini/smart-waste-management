from django.conf.urls import url
from .views import index
from .views import list_location
from .views import list_worker


urlpatterns = [
url(r'^$', index, name='index'),
url(r'list-location', list_location, name='list-location'),
url(r'list-worker', list_worker, name='list-worker'),

]
