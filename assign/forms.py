from django import forms

class add_form(forms.Form):
    Area_attrs = {
        'type': 'text',
        'class': 'add-form-input',
        'placeholder':'Enter area name'
        
    }
    Location_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'add-form-textarea',
        'placeholder':'Enter location'
        
}

    Area = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=Area_attrs))
    Location = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=Location_attrs))


class worker_form(forms.Form):
    
    name_attrs = {
        'type': 'text',
        'class': 'inputform',
        'placeholder':'Enter name'
        }
    email_attrs = {
        'type': 'email',   
        'placeholder':'Enter email'
        
    }

   # ID = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=ID_attrs))
    name= forms.CharField(label='', required=True, widget=forms.TextInput(attrs=name_attrs))
    email= forms.EmailField(required=True, widget=forms.TextInput(attrs=email_attrs))

		
